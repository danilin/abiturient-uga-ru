<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

try
{

	$arComponentParameters = array(
		'GROUPS' => array(
		),
		'PARAMETERS' => array(
            'SEF_MODE' => array(
                'index' => array(
                    'NAME' => GetMessage('ABITURIENT_UNIVERSITIES_INDEX_PAGE'),
                    'DEFAULT' => '',
                    'VARIABLES' => array()
                ),
                'sity' => array(
                    "NAME" => GetMessage('ABITURIENT_UNIVERSITIES_SITY_PAGE'),
                    "DEFAULT" => '#TYPE_ID#/',
                    "VARIABLES" => array('TYPE_ID')
                ),
                'universities' => array(
                    "NAME" => GetMessage('ABITURIENT_UNIVERSITIES_UNIVERSITIES_PAGE'),
                    "DEFAULT" => '#TYPE_ID#/#SITY_ID#/',
                    "VARIABLES" => array('TYPE_ID','SITY_ID')
                ),
                'universitie' => array(
                    "NAME" => GetMessage('ABITURIENT_UNIVERSITIES_UNIVERSITIE_PAGE'),
                    "DEFAULT" => '#TYPE_ID#/#SITY_ID#/#UNIVERSITIE_ID#/',
                    "VARIABLES" => array('TYPE_ID','SITY_ID','UNIVERSITIE_ID')
                ),
            ),
		)
	);
}
catch (Main\LoaderException $e)
{
	ShowError($e->getMessage());
}
?>