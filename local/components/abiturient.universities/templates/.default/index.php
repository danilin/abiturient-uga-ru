<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<?$APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "",
    Array(
    ),$component
);?>

<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "sity", Array(
    "IBLOCK_TYPE" => "vuz",	// Тип инфоблока
    "IBLOCK_ID" => "3",	// Инфоблок
    "SECTION_ID" => "",	// ID раздела
    "SECTION_CODE" => "",	// Код раздела
    "COUNT_ELEMENTS" => "Y",	// Показывать количество элементов в разделе
    "TOP_DEPTH" => "2",	// Максимальная отображаемая глубина разделов
    "SECTION_FIELDS" => array(	// Поля разделов
        0 => "",
        1 => "",
    ),
    "SECTION_USER_FIELDS" => array(	// Свойства разделов
        0 => "",
        1 => "",
    ),
    "SECTION_URL" => "/catalog/#SECTION_CODE#/",	// URL, ведущий на страницу с содержимым раздела
    "CACHE_TYPE" => "A",	// Тип кеширования
    "CACHE_TIME" => "36000",	// Время кеширования (сек.)
    "CACHE_GROUPS" => "N",	// Учитывать права доступа
    "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
    "VIEW_MODE" => "LINE",	// Вид списка подразделов
    "SHOW_PARENT_NAME" => "N",	// Показывать название раздела
),
    $component
);?>