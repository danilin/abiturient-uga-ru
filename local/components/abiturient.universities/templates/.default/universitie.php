<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 05.07.2018
 * Time: 2:29
 */?>
<?$APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "",
    Array(
    ),$component
);?>

<?
$IBLOCK_ID=3;
if (CModule::IncludeModule("iblock")) {
    $arFilter = Array('IBLOCK_ID' => $IBLOCK_ID, 'ACTIVE' => 'Y', 'CODE' => $arResult["VARIABLES"]['TYPE_ID']);
    $db_list = CIBlockSection::GetList(Array($by => $order), $arFilter, true);
    if ($ar_result = $db_list->GetNext())
        $APPLICATION->AddChainItem($ar_result["NAME"], $ar_result["SECTION_PAGE_URL"]);
    $arProperties = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC", "VALUE"=>"ASC"), Array("ID"=>$arResult["VARIABLES"]["SITY_ID"],"IBLOCK_ID" => $IBLOCK_ID));
    if ($arProperty = $arProperties->Fetch())
        $APPLICATION->AddChainItem($arProperty["VALUE"], "/catalog/{$arResult["VARIABLES"]["TYPE_ID"]}/{$arResult["VARIABLES"]["SITY_ID"]}/");
}
?>

<?$ElementID=$APPLICATION->IncludeComponent("bitrix:news.detail", ".default", array(
    "IBLOCK_TYPE" => "vuz",
    "IBLOCK_ID" => "3",
    "ELEMENT_ID" => "",
    "ELEMENT_CODE" => $arResult["VARIABLES"]["UNIVERSITIE_ID"],
    "CHECK_DATES" => "Y",
    "FIELD_CODE" => array(
        0 => "",
        1 => "",
    ),
    "PROPERTY_CODE" => array(
        0 => "",
        1 => "",
    ),
    "IBLOCK_URL" => "",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "36000",
    "CACHE_GROUPS" => "Y",
    "META_KEYWORDS" => "-",
    "META_DESCRIPTION" => "-",
    "BROWSER_TITLE" => "-",
    "SET_STATUS_404" => "N",
    "SET_TITLE" => "Y",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "ADD_ELEMENT_CHAIN" => "Y",
    "ACTIVE_DATE_FORMAT" => "d.m.Y",
    "USE_PERMISSIONS" => "N",
    "PAGER_TEMPLATE" => ".default",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "N",
    "PAGER_TITLE" => "Страница",
    "PAGER_SHOW_ALL" => "N",
    "DISPLAY_DATE" => "Y",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "USE_SHARE" => "N",
    "AJAX_OPTION_ADDITIONAL" => ""
),
    $component
);?>
