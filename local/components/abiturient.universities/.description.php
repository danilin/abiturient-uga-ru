<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	"NAME" => Loc::getMessage('ABITURIENT_UNIVERSITIES_DESCRIPTION_NAME'),
	"DESCRIPTION" => Loc::getMessage('ABITURIENT_UNIVERSITIES_DESCRIPTION_DESCRIPTION'),
	"SORT" => 10,
	"PATH" => array(
		"ID" => 'abiturient_universities',
		"NAME" => Loc::getMessage('ABITURIENT_UNIVERSITIES_DESCRIPTION_GROUP'),
		"SORT" => 10
	),
);

?>