<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div class="gfilter">
    <?$APPLICATION->IncludeFile(
        "/include_area/menu.php",
        Array(),
        Array("SHOW_BORDER"=>false,"MODE"=>"php")
    );?>
    <?$APPLICATION->IncludeFile(
        "/include_area/news.php",
        Array(),
        Array("SHOW_BORDER"=>false,"MODE"=>"php")
    );?>
    <div class="left">
        <div class="title">Выберите тип учебного заведения:</div>
        <ul>
            <?foreach ($arResult["SECTIONS"] as $section){?>
                <li class=" "><a href="<?=$section["SECTION_PAGE_URL"]?>"><?=$section["NAME"]?></a></li>
            <?}?>
        </ul>
    </div>

</div>
