<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title><?$APPLICATION->ShowTitle()?></title>
		<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/slides.js"></script>
		<?$APPLICATION->ShowHead();?>
	</head>
<body>
<?$APPLICATION->ShowPanel();?>
	<div class="head-back">
		<div class="head-face">
			<a class="indexlink" href="/"></a>
			<div class="search">
			<form method="POST" action="/catalog/">
				<input type="text" name="catnamefill"placeholder="Поиск по сайту" value="">
				<input type="submit" value=" ">
			</form>
			</div>
			<a class="prinv" href="/pechatnaya-versiya/"></a>
		</div>
	</div>
	<div class="cblock">
		<div class="top-menu">
			<div class="menuw">
				<?$APPLICATION->IncludeComponent("bitrix:menu", "template1", array(
	"ROOT_MENU_TYPE" => "top",
	"MENU_CACHE_TYPE" => "A",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
		0 => "",
	),
	"MAX_LEVEL" => "1",
	"CHILD_MENU_TYPE" => "",
	"USE_EXT" => "N",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>
			</div>
		</div>

		<?$page = $APPLICATION->GetCurDir();
		if (($APPLICATION->GetCurDir() == "/") || (stristr($page,'catalog'))):?>
		<div class="catalogmenu">
				<ul>
					<?
					 if(CModule::IncludeModule("iblock"))
					   { 
					     $arFilter = Array('IBLOCK_ID'=> 3);
						  $db_list = CIBlockSection::GetList(Array("ID"=>"ASC"), $arFilter, false);
						  while($ar_result = $db_list->GetNext())
						  {
							 echo '<li><a href="'.$ar_result['SECTION_PAGE_URL'].'">'.$ar_result['NAME'].'</a></li>';//print_r($ar_result);
						  }
					   }
					?>
				</ul>
		</div>
		<?endif;?>
		<script type="text/Javascript">
			$(function(){
					$("#slides").slides({
						play: 500,
						width:959,
						height:200,
					});
			});
		</script>
		<?/*if ($APPLICATION->GetCurDir() == "/"):?>
		<div id="slides" class="slider">
			<img src="<?=SITE_TEMPLATE_PATH?>/slide.jpg"/>
			<img src="<?=SITE_TEMPLATE_PATH?>/slide.jpg"/>
			<img src="<?=SITE_TEMPLATE_PATH?>/slide.jpg"/>
			<img src="<?=SITE_TEMPLATE_PATH?>/slide.jpg"/>
		</div>
		<?endif;*/?>
                <div class="content">
              