<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
foreach($arResult["ITEMS"] as &$arItem){
    if(is_array($arItem["PREVIEW_PICTURE"])){
        $arFileTmp=CFile::ResizeImageGet(
            $arItem["PREVIEW_PICTURE"]["ID"],
            array("width" => 83,"height" =>79),
            BX_RESIZE_IMAGE_EXACT,
            true
        );
        $arItem["PREVIEW_PICTURE"]["WIDTH"]=$arFileTmp["width"];
        $arItem["PREVIEW_PICTURE"]["HEIGHT"]=$arFileTmp["height"];
        $arItem["PREVIEW_PICTURE"]["SRC"]=$arFileTmp["src"];
    }
}unset($arItem);